const initialState = {
  view: {
    home: false,
    one: false,
    two: false,
    notfound: false
  }
}

export default function appReducer(state = initialState, action) {

  switch (action.type) {

    case 'TURN_HOME_PAGE_ON': {

      const view = {
        ...state.view,
        home: true,
        one: false,
        two: false,
        notfound: false
      }
      return { ...state, view }
    }

    case 'TURN_ONE_PAGE_ON': {
      console.log("AQUI")
      const view = {
        ...state.view,
        home: false,
        one: true,
        two: false,
        notfound: false
      }
      return { ...state, view }
    }

    case 'TURN_TWO_PAGE_ON': {
      const view = {
        ...state.view,
        home: false,
        one: false,
        two: true,
        notfound: false
      }
      return { ...state, view }
    }

    case 'TURN_NOTFOUND_ON': {
      const view = {
        ...state.view,
        home: false,
        one: false,
        two: false,
        notfound: true
      }
      return { ...state, view }
    }

    default: {
      return state
    }
  }
}

import store from './store'
import route from 'riot-route'
import { takeEvery, call, all } from 'redux-saga/effects'
import catchErrors from './handlers/error-handler'
import * as appController from './app-controller'

const executeRoute = (...params) => {

  const query = route.query()
  // If query is an empty object, return original params
  // If query exists, remove the last params item (that is the query)
  if (Object.keys(query).length !== 0) {
    params.pop()
  }

  return store.dispatch({
    type: 'ROUTE',
    payload: {
      params,
      query
    }
  })
}

// start and configure riot-route
route.base('#/')
route(executeRoute) // for each route change, run executeRoute function above who will dispatch an action with the type 'ROUTE'
route.start(true)

function* handleRoute(action) {

  switch (action.payload.params[0]) {

    case '':
    case 'home': {
      yield call(appController.showHomePage, action)
      break
    }

    case 'one': {
      yield call(appController.showPageOne, action)
      break
    }

    case 'two': {
      yield call(appController.showPageTwo, action)
      break
    }

    case 'notfound': {
      yield call(appController.showNotfound, action)
      break
    }

    default: {
      throw 404
    }
  }
}

function* sagaRoute() {
  yield takeEvery('ROUTE', catchErrors, handleRoute) // for every dispatch with the type ROUTE will run the handleRoute function
}

export default function* rootSaga() {
  yield all([call(sagaRoute)])
}

import rootReducer from './root-reducer'
import rootSaga from './root-saga-router'
import { applyMiddleware, createStore } from 'redux'
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware()

// Before running a Saga, you must mount the Saga middleware on the Store using applyMiddleware
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)

export default store


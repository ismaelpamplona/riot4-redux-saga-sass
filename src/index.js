import '@riotjs/hot-reload'
import './img/my-logo.png'
import { component, install } from 'riot'
import store from './store'
import App from './app.riot'

install(function(component) {
  component.store = { ...store }
})

component(App)(document.getElementById('root'))






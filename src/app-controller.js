import { put } from 'redux-saga/effects'

export function* showHomePage(action) {
  yield put({
    type: 'TURN_HOME_PAGE_ON'
  })
}

export function* showPageOne(action) {
  yield put({
    type: 'TURN_ONE_PAGE_ON'
  })
}

export function* showPageTwo(action) {
  yield put({
    type: 'TURN_TWO_PAGE_ON'
  })
}

export function* showNotfound(action) {
  yield put({
    type: 'TURN_NOTFOUND_ON'
  })
}
